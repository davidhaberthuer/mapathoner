# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][] and this project adheres to
[Semantic Versioning][].

[Keep a Changelog]: http://keepachangelog.com/
[Semantic Versioning]: http://semver.org/

## [Unreleased][]
### Changed
- Design of languages list in hugo pages.

## [0.5.17][]
### Changed
- Bump German translations.

## [0.5.16][]
### Changed
- Bump Italian translations.

## [0.5.15][]
### Added
- Italian translations.

## [0.5.14][]
### Changed
- Bump Russian translations.

## [0.5.13][]
### Added
- Polish translations.

## [0.5.12][]
### Changed
- Bump Greek translations.

## [0.5.11][]
### Fixed
- Missing gettext package in GitLab CI/CD.

## [0.5.10][]
### Added
- Greek translations for pages.

## [0.5.9][] - 2018-06-26

## [0.5.8][] - 2018-06-24
### Added
- German translations for pages.

## [0.5.7][] - 2018-06-22
### Fixed
- Language-dependent link to home.
- Last building not created bug.

## [0.5.6][] - 2018-06-22
### Changed
- Revert d1526c5 because Hugo creates `-d` output folder relative to `-s`
  source folder.

### Fixed
- Header partial of Hugo Coder theme, use `.Title` for `.Page`.

## [0.5.5][] - 2018-06-22
### Fixed
- Link to JavaDoc in Hugo Coder theme header.

## [0.5.4][] - 2018-06-22
### Added
- Russian translations for pages.

### Changed
- Change Hugo Coder theme to automatically show available content. This solve
  the problem with i18n because page not available in desired language is not
  shown.

### Fixed
- GitLab CI/CD `./public` folder location.

## [0.5.3][] - 2018-06-22
### Added
- Default content language.

## [0.5.2][] - 2018-06-21
### Added
- Transifex translation info on intro page.

### Fixed
- Closed way error.

## [0.5.1][]
### Fixed
- Multilingual navigation on all pages.

## [0.5.0][] - 2018-06-20
### Added
- Transifex push/pull settings for Hugo content.

### Changed
- Hugo Coder theme include multilingual.

### Fixed
- Markdown documentation links syntax.

## [0.4.2][] - 2018-06-11
### Fixed
- GitLab CI apt-get update.

## [0.4.1][] - 2018-06-11
### Fixed
- GitLab CI to include transifex when release.

## [0.4.0][] - 2018-06-11
### Added
- Batch L-shaped building action.
- The *mapathoner* plugin is already translated on [Transifex][] to the
  following languages:
  - English (en)[source language]
  - German (de)
  - Hungarian (hu)
  - Russian (ru)

### Changed
- Update Gradle JOSM plugin.
- Upload translations to Transifex.
- Automatically download translations from Transifex when building `.jar`.

[Transifex]: https://www.transifex.com/josm/josm/josm-plugin_mapathoner/

## [0.3.0][] - 2018-06-06
### Changed
- Project moved to GitLab.
- Update Gradle JOSM plugin by [@floscher][].

[@floscher]: https://gitlab.com/floscher

## [0.2.4][] - 2018-05-10
### Fixed
- Undo/Redo for Pick Residential Area action.
- Undo/Redo for Batch Orthogonalize Building action.
- Undo/Redo for Batch Circle Building action.

## [0.2.3][] - 2018-05-08
### Fixed
- Nodes count check for circle and orthogonal batch buildings.

## [0.2.2][] - 2018-05-06
### Fixed
- Navigation title link in Hugo Coder theme.

## [0.2.1][] - 2018-05-06
### Fixed
- Https in baseURL link.

## [0.2.0][] - 2018-05-06
### Added
- Mapathoner package JavaDoc.
- Hugo template.
- Hugo Coder theme template.
- Home, Usage, and Credits pages.

### Changed
- Update readme documentation.

## 0.1.0 - 2018-05-05
### Added
- Changelog, license, readme.
- Gradle JOSM plugin demo.
- Batch Circle Building action.
- Batch Orthogonal Building action.
- Pick Residential Area action.
- Mapathoner main class.

### Changed
- Update `build.gradle` with actual project info.

[Unreleased]: https://gitlab.com/qeef/mapathoner/compare/v0.5.17...HEAD
[0.5.17]: https://gitlab.com/qeef/mapathoner/compare/v0.5.16...v0.5.17
[0.5.16]: https://gitlab.com/qeef/mapathoner/compare/v0.5.15...v0.5.16
[0.5.15]: https://gitlab.com/qeef/mapathoner/compare/v0.5.14...v0.5.15
[0.5.14]: https://gitlab.com/qeef/mapathoner/compare/v0.5.13...v0.5.14
[0.5.13]: https://gitlab.com/qeef/mapathoner/compare/v0.5.12...v0.5.13
[0.5.12]: https://gitlab.com/qeef/mapathoner/compare/v0.5.11...v0.5.12
[0.5.11]: https://gitlab.com/qeef/mapathoner/compare/v0.5.10...v0.5.11
[0.5.10]: https://gitlab.com/qeef/mapathoner/compare/v0.5.9...v0.5.10
[0.5.9]: https://gitlab.com/qeef/mapathoner/compare/v0.5.8...v0.5.9
[0.5.8]: https://gitlab.com/qeef/mapathoner/compare/v0.5.7...v0.5.8
[0.5.7]: https://gitlab.com/qeef/mapathoner/compare/v0.5.6...v0.5.7
[0.5.6]: https://gitlab.com/qeef/mapathoner/compare/v0.5.5...v0.5.6
[0.5.5]: https://gitlab.com/qeef/mapathoner/compare/v0.5.4...v0.5.5
[0.5.4]: https://gitlab.com/qeef/mapathoner/compare/v0.5.3...v0.5.4
[0.5.3]: https://gitlab.com/qeef/mapathoner/compare/v0.5.2...v0.5.3
[0.5.2]: https://gitlab.com/qeef/mapathoner/compare/v0.5.1...v0.5.2
[0.5.1]: https://gitlab.com/qeef/mapathoner/compare/v0.5.0...v0.5.1
[0.5.0]: https://gitlab.com/qeef/mapathoner/compare/v0.4.2...v0.5.0
[0.4.2]: https://gitlab.com/qeef/mapathoner/compare/v0.4.1...v0.4.2
[0.4.1]: https://gitlab.com/qeef/mapathoner/compare/v0.4.0...v0.4.1
[0.4.0]: https://gitlab.com/qeef/mapathoner/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.com/qeef/mapathoner/compare/v0.2.4...v0.3.0
[0.2.4]: https://gitlab.com/qeef/mapathoner/compare/v0.2.3...v0.2.4
[0.2.3]: https://gitlab.com/qeef/mapathoner/compare/v0.2.2...v0.2.3
[0.2.2]: https://gitlab.com/qeef/mapathoner/compare/v0.2.1...v0.2.2
[0.2.1]: https://gitlab.com/qeef/mapathoner/compare/v0.2.0...v0.2.1
[0.2.0]: https://gitlab.com/qeef/mapathoner/compare/v0.1.0...v0.2.0
