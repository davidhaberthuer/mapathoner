# Mapathoner
[Mapathoner][] is a [JOSM][] plugin with some useful tools for [HOT][] and
[Missing Maps][] mappers.

See the [Usage][] page for how to install and use the plugin. This project is
developed under [GNU GPLv3 license][].

[Mapathoner]: https://qeef.gitlab.io/mapathoner/
[JOSM]: https://josm.openstreetmap.de/
[HOT]: https://www.hotosm.org/
[Missing Maps]: http://www.missingmaps.org/
[Usage]: https://qeef.gitlab.io/mapathoner/usage/
[GNU GPLv3 license]: ./LICENSE

# Contribute
See the [Issues][] for any kind of reports. All the feedback is greatly
appreticiated! All the discussion should be under the proper issue.

## Report [bug][]
For misbehaviour, malfunction and all the other defects use the [bug][] label.

## Propose [feature][]
For enhancements and proposals use the [feature][] label.

## Translate
All the strings are on [Transifex][]. Check out the [Markdown syntax][] for
translating the pages. Also, wrap text at 80 characters.

[Issues]: https://gitlab.com/qeef/mapathoner/issues
[bug]: https://gitlab.com/qeef/mapathoner/labels/bug
[feature]: https://gitlab.com/qeef/mapathoner/labels/feature
[Markdown syntax]: https://daringfireball.net/projects/markdown/syntax

# Develop
For quick orientation see the [changelog][]. For the generated documentation
see the [JavaDoc][] page.

[Gradle JOSM plugin][] is used for the development. Source code adheres to
[JOSM Development Guidelines][].

## Commit message
- Separate subject from body with a blank line.
- Limit the subject line to 50 characters.
- Capitalize the subject line.
- Do not end the subject line with a period.
- Use the imperative mood in the subject line.
- Wrap the body at 72 characters.
- Use the body to explain *what* and *why* vs. *how*.

See [The seven rules of a great Git commit message][] for more info.

## Branching model
To solve the [bug][], checkout the `hotfix/X.Y.Z` branch from *the last
version tag*.

To code the [feature][], checkout the `feature/WHATEVER` branch from `master`.
Before merge request, `feature/WHATEVER` branch *must be rebased on `master`*.

If only new translations are added to [Transifex][], checkout `transfix/X.Y.Z`
branch from *the last version tag*.

See [OneFlow][] for more info.

## Pages
[Hugo][] with slightly modified [Coder][] theme is used for pages generation.
Pages are hosted as [GitLab pages][].

Translations are merged when 100% rewied all the [JOSM resources][] related to
mapathoner. Before merge, translated files are checked by [charsorder.py][].
Usable translations yet not 100% or rewied are still used due to CI/CD.

[changelog]: ./CHANGELOG.md
[JavaDoc]: https://qeef.gitlab.io/mapathoner/javadoc/
[Gradle JOSM plugin]: https://plugins.gradle.org/plugin/org.openstreetmap.josm
[JOSM Development Guidelines]: https://josm.openstreetmap.de/wiki/DevelopersGuide/StyleGuide/
[The seven rules of a great Git commit message]: https://chris.beams.io/posts/git-commit/
[bug]: https://gitlab.com/qeef/mapathoner/labels/bug
[feature]: https://gitlab.com/qeef/mapathoner/labels/feature
[Transifex]: https://www.transifex.com/josm/josm/josm-plugin_mapathoner/
[OneFlow]: http://endoflineblog.com/oneflow-a-git-branching-model-and-workflow
[Hugo]: http://gohugo.io/
[Coder]: https://github.com/luizdepra/hugo-coder
[GitLab pages]: https://docs.gitlab.com/ee/user/project/pages/
[JOSM resources]: https://www.transifex.com/josm/josm/content/
[charsorder.py]: https://gitlab.com/qeef/charsorder.py

*Keep mapping!*
