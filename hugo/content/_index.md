---
title: ""
date: 2018-05-06T11:40:10+02:00
---
# What if ...
- You create multiple buildings by one way?
- Create residential area by selecting the buildings?

![Mapathoner demo](https://upload.wikimedia.org/wikipedia/commons/9/9f/Mapathoner_demo.gif)

See the project on [GitLab](https://gitlab.com/qeef/mapathoner) or translate on
[Transifex](https://www.transifex.com/josm/josm/content/).
