// License: GPL. For details, see LICENSE file.
package org.openstreetmap.josm.plugins.mapathoner;

import static org.openstreetmap.josm.gui.help.HelpUtil.ht;
import static org.openstreetmap.josm.tools.I18n.tr;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.openstreetmap.josm.Main;
import org.openstreetmap.josm.actions.JosmAction;
import org.openstreetmap.josm.actions.OrthogonalizeAction;
import org.openstreetmap.josm.command.AddCommand;
import org.openstreetmap.josm.command.ChangePropertyCommand;
import org.openstreetmap.josm.command.Command;
import org.openstreetmap.josm.command.DeleteCommand;
import org.openstreetmap.josm.command.RemoveNodesCommand;
import org.openstreetmap.josm.command.SequenceCommand;
import org.openstreetmap.josm.data.coor.EastNorth;
import org.openstreetmap.josm.data.osm.DataSet;
import org.openstreetmap.josm.data.osm.Node;
import org.openstreetmap.josm.data.osm.OsmPrimitive;
import org.openstreetmap.josm.data.osm.Way;
import org.openstreetmap.josm.gui.MainApplication;
import org.openstreetmap.josm.gui.Notification;
import org.openstreetmap.josm.tools.ImageProvider;
import org.openstreetmap.josm.tools.Shortcut;

/**
 * Create multiple L-shaped buildings based on nodes of a way.
 * <p>
 * Usage:
 * <ul>
 * <li>Creates a way that consists of nodes at the building corners.
 * <li>Runs action.
 * </ul>
 * <p>
 * Performs:
 * <ul>
 * <li>From quadruplets of way nodes creates orthogonalized L-shape.
 * <li>Tags the L-shape as building=yes.
 * </ul>
 * <p>
 * Notes:
 * <ul>
 * <li>Useful for HOT and Missing Maps mappers.
 * <li>This action uses <code>OrthogonalizeAction</code>.
 * </ul>
 *
 * @author qeef
 * @since xxx
 */

public final class BatchLBuildingAction extends JosmAction
{
    /**
     * Constructs a new {@code BatchLBuildingAction}.
     */
    public BatchLBuildingAction()
    {
        super(tr("Batch L-shaped Building"),
                (ImageProvider) null,
                tr("Create multiple L-shaped buildings from a way."),
                Shortcut.registerShortcut("mapathoner:batchlbuilding",
                    tr("Mapathoner: {0}", tr("Batch L-shaped Building")),
                    KeyEvent.VK_L,
                    Shortcut.CTRL_SHIFT),
                true,
                "batchlbuilding",
                true);
        putValue("help", ht("/Action/BatchLBuilding"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (!isEnabled())
            return;

        DataSet ds = getLayerManager().getEditDataSet();
        if (ds == null)
            return;

        Collection<OsmPrimitive> sel = ds.getSelected();
        List<Way> ways = OsmPrimitive.getFilteredList(sel, Way.class);

        Way existingWay = null;
        int i;

        if (ways.size() != 1) {
            new Notification(
                    tr("Please create a way with nodes count divisible by 4."))
                    .setIcon(JOptionPane.INFORMATION_MESSAGE)
                    .setDuration(Notification.TIME_LONG)
                    .show();
            return;
        }

        existingWay = ways.get(0);
        if (existingWay.getNodesCount() < 4) {
            new Notification(
                    tr("Please create a way with nodes count divisible by 4."))
                    .setIcon(JOptionPane.INFORMATION_MESSAGE)
                    .setDuration(Notification.TIME_LONG)
                    .show();
            return;
        }

        List<Node> nodes = existingWay.getNodes();
        List<EastNorth> ens = new ArrayList<EastNorth>();
        Map<String, String> tag_by = new HashMap<String, String>();
        tag_by.put("building", "yes");
        Collection<Way> lbuildings = new LinkedList<Way>();
        Collection<Command> cmds = new LinkedList<Command>();
        for (i = 0; i < nodes.size(); i++) {
            if (nodes.get(i) == nodes.get(nodes.size() - 1) &&
                    i != nodes.size() - 1) {
                nodes.remove(nodes.size() - 1);
                break;
            }
        }
        for (Node n : nodes) {
            ens.add(n.getEastNorth());
        }

        cmds.add(new RemoveNodesCommand(existingWay, nodes));
        cmds.add(new DeleteCommand(ds, existingWay));
        for (Node n : nodes) {
            cmds.add(new DeleteCommand(ds, n));
        }

        if (ens.size() % 4 != 0) {
            ens.remove(ens.size() - 1);
        }
        if (ens.size() % 4 != 0) {
            ens.remove(ens.size() - 1);
        }
        if (ens.size() % 4 != 0) {
            ens.remove(ens.size() - 1);
        }

        for (i = 0; i < ens.size() - 3; i += 4) {
            List<Node> lb_nodes = new ArrayList<Node>();
            Way lb_way = new Way();

            EastNorth n0 = ens.get(i);
            EastNorth n1 = ens.get(i + 1);
            EastNorth n2 = ens.get(i + 2);
            EastNorth n3 = ens.get(i + 3);

            double dotp;
            double f;

            // Compute dot product, select (lines) pair with the lowest one
            // to find out which (lines) pair is the closest one to right
            // angle.
            //
            // see https://www.mathsisfun.com/algebra/vectors-dot-product.html
            dotp = (n3.east() - n0.east()) * (n1.east() - n2.east()) -
                (n3.north() - n0.north()) * (n1.north() - n2.north());
            dotp = Math.abs(dotp);
            EastNorth p0 = n3;
            EastNorth p1 = n0;
            EastNorth p2 = n1;
            EastNorth p3 = n2;

            f = (n3.east() - n1.east()) * (n0.east() - n2.east()) -
                (n3.north() - n1.north()) * (n0.north() - n2.north());
            f = Math.abs(f);
            if (f < dotp) {
                dotp = f;
                p0 = n3;
                p1 = n1;
                p2 = n0;
                p3 = n2;
            }

            f = (n3.east() - n2.east()) * (n0.east() - n1.east()) -
                (n3.north() - n2.north()) * (n0.north() - n1.north());
            f = Math.abs(f);
            if (f < dotp) {
                dotp = f;
                p0 = n3;
                p1 = n2;
                p2 = n0;
                p3 = n1;
            }

            // Compute intersection of the lines.
            // see https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection
            double deno;
            deno = (p0.east() - p1.east()) * (p2.north() - p3.north()) -
                (p0.north() - p1.north()) * (p2.east() - p3.east());
            if (deno == 0) {
                new Notification(
                        tr("Denominator is 0! This should not happen!"))
                        .setIcon(JOptionPane.INFORMATION_MESSAGE)
                        .setDuration(Notification.TIME_LONG)
                        .show();
                return;
            }

            double px;
            double py;
            px = (p0.east() * p1.north() - p0.north() * p1.east()) *
                (p2.east() - p3.east()) - (p0.east() - p1.east()) *
                (p2.east() * p3.north() - p2.north() * p3.east());
            px /= deno;
            py = (p0.east() * p1.north() - p0.north() * p1.east()) *
                (p2.north() - p3.north()) - (p0.north() - p1.north()) *
                (p2.east() * p3.north() - p2.north() * p3.east());
            py /= deno;

            // Order by distance to intersection (px, py).
            EastNorth tmp;
            f = (px - p0.east()) * (px - p0.east()) +
                (py - p0.north()) * (py - p0.north());
            if ((px - p1.east()) * (px - p1.east()) +
                    (py - p1.north()) * (py - p1.north()) < f) {
                tmp = p0;
                p0 = p1;
                p1 = tmp;
            }
            f = (px - p2.east()) * (px - p2.east()) +
                (py - p2.north()) * (py - p2.north());
            if ((px - p3.east()) * (px - p3.east()) +
                    (py - p3.north()) * (py - p3.north()) < f) {
                tmp = p2;
                p2 = p3;
                p3 = tmp;
            }

            // Find out the rest nodes.
            EastNorth p4 = new EastNorth(p0.east() + p2.east() - px,
                    p0.north() + p2.north() - py);
            EastNorth p5 = new EastNorth(p1.east() + p3.east() - px,
                    p1.north() + p3.north() - py);

            // Draw the L-shaped building.
            lb_nodes.add(new Node(p0));
            lb_nodes.add(new Node(p4));
            lb_nodes.add(new Node(p2));
            lb_nodes.add(new Node(p3));
            lb_nodes.add(new Node(p5));
            lb_nodes.add(new Node(p1));

            for (Node n: lb_nodes) {
                cmds.add(new AddCommand(ds, n));
            }

            lb_nodes.add(lb_nodes.get(0));
            lb_way.setNodes(lb_nodes);
            cmds.add(new AddCommand(ds, lb_way));

            lbuildings.add(lb_way);
        }

        cmds.add(new ChangePropertyCommand(ds, lbuildings, tag_by));
        MainApplication.undoRedo.add(new SequenceCommand(
                    tr("Batch L-shaped Building"), cmds));

        OrthogonalizeAction oa = new OrthogonalizeAction();
        for (Way w: lbuildings) {
            ds.clearSelection();
            ds.addSelected(w);
            oa.actionPerformed(null);
            ds.clearSelection();
        }
    }
}
